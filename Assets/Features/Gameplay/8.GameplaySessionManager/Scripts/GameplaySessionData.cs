using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameplaySessionData", menuName = "GD/GameplaySessionData")]
public class GameplaySessionData : ScriptableObject
{
    [SerializeField] public float gameplaySessionDuration;
}
