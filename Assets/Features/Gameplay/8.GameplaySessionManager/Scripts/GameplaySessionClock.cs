using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameplaySessionClock : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI clockText;

    public void UpdateTime(float totalSeconds)
    {
        int minutes = Mathf.FloorToInt(totalSeconds / 60F);
        int seconds = Mathf.FloorToInt(totalSeconds - minutes * 60);
        clockText.text = string.Format("{0:0}:{1:00}", minutes, seconds);
    }
}
