using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplaySessionManager : MonoBehaviour
{
    [SerializeField] GameplaySessionData gameplaySessionData;
    [SerializeField] GameplaySessionClock gameplaySessionClock;
    [SerializeField] CanvasGroup finalResultPanel;
    [SerializeField] CanvasGroup menuPanel;
    [SerializeField] CanvasGroup optionsPanel;
    [SerializeField] Button restartButton;
    [SerializeField] Button menuButton;
    [SerializeField] TMP_InputField gameSessionTimeInputField;
    [SerializeField] TMP_InputField enemySpawnTimeInputField;
    [SerializeField] EnemyManager enemyManager;
    [SerializeField] Button playButton;
    [SerializeField] Button optionsButton;
    [SerializeField] Player player;

    float currentSessionDuration;
    Coroutine SessionTimeCounterRoutine;

    private void OnEnable()
    {
        Time.timeScale = 1;

        gameSessionTimeInputField.text = gameplaySessionData.gameplaySessionDuration.ToString();
        currentSessionDuration = gameplaySessionData.gameplaySessionDuration;

        enemySpawnTimeInputField.text = enemyManager.EnemyManagerData.baseSpawnCooldown.ToString();

        gameSessionTimeInputField.onValueChanged.AddListener(GameSessionTimeInputFieldOnValueChanged);
        enemySpawnTimeInputField.onValueChanged.AddListener(EnemySpawnTimeInputFieldOnValueChanged);

        restartButton.onClick.AddListener(Restart);
        menuButton.onClick.AddListener(Menu);

        SessionTimeCounterRoutine = StartCoroutine(SessionTimeCounter());

        playButton.onClick.AddListener(OnPlayButtonPressed);
        optionsButton.onClick.AddListener(OnOptionsButtonPressed);

        player.OnDeathEvent += OnPlayerDeath;
    }

    void OnPlayerDeath()
    {
        OnEndSession();
    }

    private void OnDisable()
    {
        restartButton.onClick.RemoveListener(Restart);
        menuButton.onClick.RemoveListener(Menu);
        gameSessionTimeInputField.onValueChanged.RemoveListener(GameSessionTimeInputFieldOnValueChanged);
        enemySpawnTimeInputField.onValueChanged.RemoveListener(EnemySpawnTimeInputFieldOnValueChanged);

        playButton.onClick.RemoveListener(OnPlayButtonPressed);
        optionsButton.onClick.RemoveListener(OnOptionsButtonPressed);

        player.OnDeathEvent -= OnPlayerDeath;
    }

    void OnPlayButtonPressed()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void OnOptionsButtonPressed()
    {
        OpenPanel(optionsPanel);
    }

    void GameSessionTimeInputFieldOnValueChanged(string _text)
    {
        try
        {
            var newGameSessionTime = int.Parse(_text);
            gameplaySessionData.gameplaySessionDuration = newGameSessionTime;
        }
        catch (System.Exception ex)
        {
        }
    }
    void EnemySpawnTimeInputFieldOnValueChanged(string _text)
    {
        try
        {
            var newBaseSpawnCooldown = int.Parse(_text);
            enemyManager.EnemyManagerData.baseSpawnCooldown = newBaseSpawnCooldown;
        }
        catch (System.Exception ex)
        {

        }
    }

    IEnumerator SessionTimeCounter()
    {
        while(currentSessionDuration > 0)
        {
            UpdateTimer();
            yield return null;
        }
        OnEndSession();
    }

    void UpdateTimer()
    {
        currentSessionDuration -= Time.deltaTime;
        currentSessionDuration = Mathf.Max(0, currentSessionDuration);
        gameplaySessionClock.UpdateTime(currentSessionDuration);
    }

    void OnEndSession()
    {
        Time.timeScale = 0;
        OpenPanel(finalResultPanel);
    }

    void OpenPanel(CanvasGroup _canvasGroup)
    {
        _canvasGroup.alpha = 1;
        _canvasGroup.interactable = true;
        _canvasGroup.blocksRaycasts = true;
    }
    void ClosePanel(CanvasGroup _canvasGroup)
    {
        _canvasGroup.alpha = 0;
        _canvasGroup.interactable = false;
        _canvasGroup.blocksRaycasts = false;
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Menu()
    {
        OpenPanel(menuPanel);
    }
}
