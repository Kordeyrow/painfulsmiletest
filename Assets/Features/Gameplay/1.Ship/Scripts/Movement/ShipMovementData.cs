using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShipMovementData
{
    [SerializeField] float moveSpeed;
    [SerializeField] float rotationSpeed;

    public float MoveSpeed => moveSpeed;
    public float RotationSpeed => rotationSpeed;
}
