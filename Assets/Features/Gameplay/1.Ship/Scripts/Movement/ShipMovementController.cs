using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovementController : MonoBehaviour
{
    const float fixedUpdateMoveSpeedDataAlteration = 0.02f;
    const float fixedUpdateMoveToSpeedDataAlteration = 0.0002f;
    const float startMovingToTargetThreshold = 1f;

    public void Move(int _moveDirection, int _rotationDirection, ShipMovementData _shipMovementData)
    {
        transform.Translate(Vector2.up * _moveDirection * _shipMovementData.MoveSpeed * fixedUpdateMoveSpeedDataAlteration, Space.Self);
        transform.Rotate(Vector3.forward *  -_rotationDirection * _shipMovementData.RotationSpeed * fixedUpdateMoveSpeedDataAlteration);
    }

    public void MoveTo(Transform _target, ShipMovementData _shipMovementData)
    {
        MoveTo(_target.position, _shipMovementData);
    }
    public void MoveTo(Vector3 _targetPos, ShipMovementData _shipMovementData)
    {
        Vector3 targetRotation = Quaternion.Euler(new Vector3(0, 0, -90)) * (_targetPos - transform.position).normalized;
        transform.right = Vector3.Slerp(transform.right, targetRotation, _shipMovementData.RotationSpeed * fixedUpdateMoveToSpeedDataAlteration);

        //Debug.Log("transform.right: " + transform.right);
        //Debug.Log("targetRotation: " + targetRotation);

        //jg diff
        var targetDirectionDiff = Vector3.Distance(transform.right, targetRotation);
        var NotLookingToTargetDirection = targetDirectionDiff > startMovingToTargetThreshold;
        if (NotLookingToTargetDirection)
            return;

        float coolSmoothSpeedForPainfulSmileTest = 1 - targetDirectionDiff / startMovingToTargetThreshold;
        transform.Translate(Vector2.up * _shipMovementData.MoveSpeed * fixedUpdateMoveSpeedDataAlteration * coolSmoothSpeedForPainfulSmileTest, Space.Self);
    }
    public void RotateTo(Vector3 _targetPos, ShipMovementData _shipMovementData)
    {
        Vector3 targetRotation = Quaternion.Euler(new Vector3(0, 0, -90)) * (_targetPos - transform.position).normalized;
        transform.right = Vector3.Slerp(transform.right, targetRotation, _shipMovementData.RotationSpeed * fixedUpdateMoveToSpeedDataAlteration);
    }
}
