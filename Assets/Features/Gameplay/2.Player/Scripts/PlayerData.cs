using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "GD/Player/PlayerData")]
public class PlayerData : ScriptableObject
{
    [SerializeField] ShipMovementData shipMovementData;
    [SerializeField] ShootData shootData;
    [SerializeField] SimpleDamagebleData simpleDamagebleData;

    public ShipMovementData ShipMovementData => shipMovementData;
    public ShootData ShootData => shootData;
    public SimpleDamagebleData SimpleDamagebleData => simpleDamagebleData;
}
