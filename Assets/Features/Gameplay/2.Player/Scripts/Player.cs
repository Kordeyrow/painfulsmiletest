using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    [SerializeField] ShipMovementController shipMovementController;
    [SerializeField] PlayerData playerData;
    [SerializeField] SimpleDamageble simpleDagamageble;
    [SerializeField] ShootController shootController;
    [SerializeField] GameObject explosionPrefab;

    public event UnityAction OnDeathEvent;

    private void OnEnable()
    {
        simpleDagamageble.Setup(playerData.SimpleDamagebleData);
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);

        shootController.Setup(playerData.ShootData);
    }
    private void OnDisable()
    {
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);
    }
    private void Update()
    {
        HandleShoot();
        //PassiveHealing();
    }
    private void FixedUpdate()
    {
        HandleMovement();
    }

    void PassiveHealing()
    {
        simpleDagamageble.AddHealth(simpleDagamageble.MaxHealth/150 * Time.deltaTime);
    }
    void HandleMovement()
    {
        shipMovementController.Move(MovementDirectionInput(), RotationDirectionInput(), playerData.ShipMovementData);
    }
    int RotationDirectionInput()
    {
        return (int)Input.GetAxisRaw("Horizontal");
    }
    int MovementDirectionInput()
    {
        return (int)Input.GetAxisRaw("Vertical");
    }


    void HandleShoot()
    {
        if (ShootInput() && shootController.CanShoot())
            Shoot();
        else if (TripleShootInput() && shootController.CanShoot())
        {
            var forward = Quaternion.Euler(0, 0, 90) * transform.right;
            shootController.TripleShoot(forward, transform.position + forward / 2, playerData.ShootData);
        }
    }
    bool ShootInput()
    {
        return Input.GetMouseButtonDown(0);
    }
    bool TripleShootInput()
    {
        return Input.GetMouseButtonDown(1);
    }
    void Shoot()
    {
        //var shootDirection = CalculateShootDirection();
        var forward = Quaternion.Euler(0, 0, 90) * transform.right;
        shootController.Shoot(forward, transform.position + forward/2, playerData.ShootData);
    }
    //Vector2 CalculateShootDirection()
    //{
    //    return GetOriginToMouseDirection(transform.position);
    //}
    //Vector2 GetOriginToMouseDirection(Vector2 _origin)
    //{
    //    var worldMousePos = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
    //    var targetDirection = worldMousePos - _origin;
    //    return targetDirection.normalized;
    //}

    void OnDeath()
    {
        OnDeathEvent?.Invoke();

        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
    }
}
