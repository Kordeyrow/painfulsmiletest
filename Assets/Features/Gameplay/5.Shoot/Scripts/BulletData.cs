using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BulletData
{
    [SerializeField] float totalTravelDistance;
    [SerializeField] float damage;
    [SerializeField] float moveSpeed;

    public float TotalTravelDistance => totalTravelDistance;
    public float Damage => damage;
    public float MovSpeed => moveSpeed;
}
