using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    ShootData shootData;
    DateTime lastShotTime;

    public bool CanShoot()
    {
        if (DateTime.Now.Subtract(lastShotTime).TotalSeconds > shootData.Cooldown)
            return true;
        return false;
    }

    public void Setup(ShootData _shootData)
    {
        shootData = _shootData;
    }

    public void Shoot(Vector2 _direction, Vector2 _spawnPoint, ShootData _shootData)
    {
        var bullet = Instantiate(_shootData.BulletPrefab, _spawnPoint, Quaternion.identity).GetComponent<Bullet>();
        bullet.OnSpawn(_direction, _shootData.BulletData, gameObject.layer);
        lastShotTime = DateTime.Now;
    }
    public void TripleShoot(Vector2 _direction, Vector2 _spawnPoint, ShootData _shootData)
    {
        var bullet = Instantiate(_shootData.BulletPrefab, _spawnPoint, Quaternion.identity).GetComponent<Bullet>();
        bullet.OnSpawn(_direction, _shootData.BulletData, gameObject.layer);
        bullet = Instantiate(_shootData.BulletPrefab, _spawnPoint + (Vector2)(Quaternion.Euler(0,0,90) * _direction/2.5f) -_direction / 2.5f, Quaternion.identity).GetComponent<Bullet>();
        bullet.OnSpawn(_direction, _shootData.BulletData, gameObject.layer);
        bullet = Instantiate(_shootData.BulletPrefab, _spawnPoint + (Vector2)(Quaternion.Euler(0, 0, -90) * _direction / 2.5f) - _direction / 2.5f, Quaternion.identity).GetComponent<Bullet>();
        bullet.OnSpawn(_direction, _shootData.BulletData, gameObject.layer);
        lastShotTime = DateTime.Now;
    }
}
