using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] GameObject explosionPrefab;

    BulletData bulletData;
    LayerMask spawnerLayer;

    Vector2 direction;
    bool destroyedSelf;
    float currentTravelledDistance;
    const float fixedUpdateMoveSpeedDataAlteration = 0.02f;

    private void FixedUpdate()
    {
        if (destroyedSelf)
            return;

        CheckForMaxTravelDistance();

        if (destroyedSelf)
            return;

        Move();
    }

    private void OnTriggerEnter2D(Collider2D _col)
    {
        var objectHead = _col.gameObject;
        var colOwner = _col.GetComponent<ColliderOwner>();
        if (colOwner != null && colOwner.Owner != null)
            objectHead = colOwner.Owner;

        OnCollision(objectHead);
    }

    public void OnSpawn(Vector2 _direction, BulletData _bulletData, LayerMask _spawnerLayer)
    {
        direction = _direction;
        bulletData = _bulletData;
        spawnerLayer = _spawnerLayer;
        SetBulletRotation();
    }

    void SetBulletRotation()
    {
        Vector3 targetRotation = Quaternion.Euler(new Vector3(0, 0, -90)) * direction.normalized;
        transform.right = targetRotation;
    }

    void CheckForMaxTravelDistance()
    {
        if (currentTravelledDistance >= bulletData.TotalTravelDistance)
        {
            DestroySelf();
            return;
        }
    }

    private void Move()
    {
        float distanceAdded = bulletData.MovSpeed * fixedUpdateMoveSpeedDataAlteration;
        transform.Translate(Vector2.up * bulletData.MovSpeed * fixedUpdateMoveSpeedDataAlteration, Space.Self);
        currentTravelledDistance += distanceAdded;
    }

    void OnCollision(GameObject _collider)
    {
        if (destroyedSelf)
            return;

        var colliderIsInSameLayerAsSpawner = _collider.gameObject.layer == spawnerLayer;
        if (colliderIsInSameLayerAsSpawner)
            return;

        DamagebleIfPossible(_collider);

        DestroySelf();

        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
    }

    void DamagebleIfPossible(GameObject _collider)
    {
        var idamageble = _collider.GetComponent<IDamageble>();
        if (idamageble != null)
            idamageble.ReceiveDamage(bulletData.Damage);
    }

    void DestroySelf()
    {
        GameObject.Destroy(this.gameObject);
        destroyedSelf = true;
    }

    //TODO: use objects pooling like enemies

    //public void OnSpawn(Vector3 _position)
    //{

    //}

    //void ResetDynamicData()
    //{
    //    currentTravelledDistance = 0;
    //}
}
