using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShootData
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] float cooldown;
    [SerializeField] BulletData bulletData;

    public GameObject BulletPrefab => bulletPrefab;
    public float Cooldown => cooldown;
    public BulletData BulletData => bulletData;
}
