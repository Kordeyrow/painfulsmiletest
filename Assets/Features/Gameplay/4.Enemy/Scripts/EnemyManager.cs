using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] EnemyManagerData enemyManagerData;
    [SerializeField] GameObject enemyShooterPrefab;
    [SerializeField] GameObject enemyChaserPrefab;
    [SerializeField] Transform enemySpawnPointsParent;
    [SerializeField] Transform targetPatrolPointsParent;
    [SerializeField] Transform player;

    public EnemyManagerData EnemyManagerData => enemyManagerData;

    List<Transform> spawnPoints;
    List<BaseEnemy> enemyPool = new List<BaseEnemy>();
    List<BaseEnemy> currentEnemies = new List<BaseEnemy>();
    float currentSpawnCooldown;

    public event UnityAction OnEnemyDeathEvent;

    private void Awake()
    {
        spawnPoints = GetAllSpawnPoints();
        currentSpawnCooldown = enemyManagerData.baseSpawnCooldown;
    }

    private void Start()
    {
        SetupEnemyPool();
        SpawnStartingEnemies();
    }

    void SetupEnemyPool()
    {
        for (int i = 0; i < enemyManagerData.MaxEnemies; i++)
        {
            var spawnPoint = ChooseRandomSpawnPoint();
            var enemyToSpawn = ChooseRandomEnemy();
            var newEnemyGO = Instantiate(enemyToSpawn, spawnPoint, Quaternion.identity);
            var newEnemy = newEnemyGO.GetComponent<BaseEnemy>();
            newEnemy.Setup(this, targetPatrolPointsParent, player.gameObject);
            newEnemyGO.SetActive(false);
            enemyPool.Add(newEnemy);
        }
    }

    GameObject ChooseRandomEnemy()
    {
        return Random.value >= 0.5f ? enemyShooterPrefab : enemyChaserPrefab;
    }

    void SpawnStartingEnemies()
    {
        for (int i = 0; i < enemyManagerData.StartEnemiesCount; i++)
        {

            Spawn();
        }
    }

    List<Transform> GetAllSpawnPoints()
    {
        var targets = new List<Transform>();
        int count = enemySpawnPointsParent.childCount;
        for (int i = 0; i < count; i++)
        {
            var child = enemySpawnPointsParent.GetChild(i);
            targets.Add(child);
        }
        return targets;
    }


    private void Update()
    {
        UpdateSpawnCooldown();

        if (ShouldSpawnNewEnemy())
        {
            Spawn();
            currentSpawnCooldown = enemyManagerData.baseSpawnCooldown;
        }
    }

    void UpdateSpawnCooldown()
    {
        currentSpawnCooldown -= Time.deltaTime;
    }

    bool ShouldSpawnNewEnemy()
    {
        if (currentSpawnCooldown > 0 
            || currentEnemies.Count >= enemyManagerData.MaxEnemies)
            return false;

        return true;
    }

    void Spawn()
    {
        if (enemyPool.Count <= 0)
            return;

        var enemyToSpawn = enemyPool[0];
        enemyPool.RemoveAt(0);
        currentEnemies.Add(enemyToSpawn);

        var spawnPoint = ChooseRandomSpawnPoint();
        enemyToSpawn.OnSpawn(spawnPoint);
    }

    Vector3 ChooseRandomSpawnPoint()
    {
        spawnPoints = spawnPoints.OrderBy(t => Vector2.Distance(t.position, player.position)).ToList();
        return spawnPoints[spawnPoints.Count - Random.Range(1, 5)].position;
    }

    public void OnEnemyDeath(BaseEnemy _enemy)
    {
        currentEnemies.Remove(_enemy);
        enemyPool.Add(_enemy);

        //Debug.Log("OnEnemyDeath");
        OnEnemyDeathEvent?.Invoke();
    }
}
