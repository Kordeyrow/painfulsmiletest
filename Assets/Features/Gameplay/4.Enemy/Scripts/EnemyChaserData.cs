using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyChaserData", menuName = "GD/Enemy/EnemyChaserData")]
public class EnemyChaserData : ScriptableObject
{
    [SerializeField] float visionRange;
    [SerializeField] float impactDamage;
    [SerializeField] ShipMovementData shipMovementData;
    [SerializeField] SimpleDamagebleData simpleDamagebleData;

    public float VisionRange => visionRange;
    public float ImpactDamage => impactDamage;
    public ShipMovementData ShipMovementData => shipMovementData;
    public SimpleDamagebleData SimpleDamagebleData => simpleDamagebleData;
}
