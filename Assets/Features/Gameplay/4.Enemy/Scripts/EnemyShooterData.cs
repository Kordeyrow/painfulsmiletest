using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyShooterData", menuName = "GD/Enemy/EnemyShooterData")]
public class EnemyShooterData : ScriptableObject
{
    [SerializeField] float visionRange;
    [SerializeField] float shottingDistance;
    [SerializeField] ShipMovementData shipMovementData;
    [SerializeField] ShootData shootData;
    [SerializeField] SimpleDamagebleData simpleDamagebleData;

    public float VisionRange => visionRange;
    public float ShottingDistance => shottingDistance;
    public ShipMovementData ShipMovementData => shipMovementData;
    public ShootData ShootData => shootData;
    public SimpleDamagebleData SimpleDamagebleData => simpleDamagebleData;
}
