using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyShooter : MonoBehaviour, BaseEnemy
{
    [SerializeField] ShipMovementController shipMovementController;
    [SerializeField] EnemyShooterData enemyData;
    [SerializeField] SimpleDamageble simpleDagamageble;
    [SerializeField] ShootController shootController;
    [SerializeField] GameObject explosionPrefab;

    GameObject player;
    Transform targetPatrolPointsParent;
    EnemyManager enemyManager;

    Transform currentTargetPoint;
    List<Transform> targetPatrolPoints;
    bool isDead;

    bool shouldShoot;

    private void OnEnable()
    {
        simpleDagamageble.Setup(enemyData.SimpleDamagebleData);
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);

        shootController.Setup(enemyData.ShootData);
    }
    private void OnDisable()
    {
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);
    }
    private void Update()
    {
        HandleShoot();
    }
    private void FixedUpdate()
    {
        HandleMovement();
    }

    void HandleShoot()
    {
        shouldShoot = ShouldShoot();
        if(shouldShoot && shootController.CanShoot())
            Shoot();
    }

    bool ShouldShoot()
    {
        return PlayerInVisionRange() && FacingPlayer(); 
    }
    bool PlayerInVisionRange()
    {
        if (player == null)
        {
            Debug.Log("player == null");
            return false;
        }
        return Vector2.Distance(player.transform.position, transform.position) < enemyData.VisionRange;
    }
    bool FacingPlayer()
    {
        Vector3 targetRotation = Quaternion.Euler(new Vector3(0, 0, -90)) * (player.transform.position - transform.position).normalized;

        //Debug.Log("transform.right: " + transform.right);
        //Debug.Log("targetRotation: " + targetRotation);

        //jg diff
        var targetDirectionDiff = Vector3.Distance(transform.right, targetRotation);
        var NotLookingToTargetDirection = targetDirectionDiff > 0.2f;
        if (NotLookingToTargetDirection)
            return false;
        return true;
    }
    void Shoot()
    {
        //var shootDirection = CalculateShootDirection();
        var forward = Quaternion.Euler(0, 0, 90) * transform.right;
        shootController.Shoot(forward, transform.position + forward/2, enemyData.ShootData);
    }

    //Vector2 CalculateShootDirection()
    //{
    //    return ((Vector2)player.transform.position - (Vector2)transform.position);
    //}

    public void Setup(EnemyManager _enemyManager, Transform _targetPointsParent, GameObject _player)
    {
        enemyManager = _enemyManager;
        targetPatrolPointsParent = _targetPointsParent;
        targetPatrolPoints = GetAllTargetPatrolPoints();
        player = _player;
    }

    List<Transform> GetAllTargetPatrolPoints()
    {
        var targets = new List<Transform>();
        int count = targetPatrolPointsParent.childCount;
        for (int i = 0; i < count; i++)
        {
            var child = targetPatrolPointsParent.GetChild(i);
            targets.Add(child);
        }
        return targets;
    }


    void HandleMovement()
    {
        if (PlayerInVisionRange())
        {
            MoveToShootingPosition();
            return;
        }

        Patrol();
    }
    void Patrol()
    {
        if (currentTargetPoint == null)
        {
            currentTargetPoint = NextTargetPoint();
            if (currentTargetPoint == null)
                return;
        }

        bool arrivedOnTargetPoint = Vector3.Distance(transform.position, currentTargetPoint.transform.position) < 0.5f;
        if (arrivedOnTargetPoint)
            currentTargetPoint = NextTargetPoint();

        shipMovementController.MoveTo(currentTargetPoint, enemyData.ShipMovementData);
    }

    void MoveToShootingPosition()
    {
        var playerDirection = ((Vector2)player.transform.position - (Vector2)transform.position).normalized;
        var shootingPosition = (Vector2)player.transform.position - playerDirection * enemyData.ShottingDistance;
        var isNearEnoughToPlayer = Vector2.Distance(transform.position, player.transform.position) <= Vector2.Distance(shootingPosition, player.transform.position);
        if (isNearEnoughToPlayer)
        {
            RotateToPlayer();
            return;
        }
        shipMovementController.MoveTo(shootingPosition, enemyData.ShipMovementData);
    }
    void RotateToPlayer()
    {
        shipMovementController.RotateTo(player.transform.position, enemyData.ShipMovementData);
    }

    Transform NextTargetPoint()
    {
        if (targetPatrolPoints == null)
        {
            Debug.Log("targetPatrolPoints == null");
            return null;
        }
        targetPatrolPoints = targetPatrolPoints.OrderBy(t => Vector2.Distance(t.position, transform.position)).ToList();
        var target = targetPatrolPoints[Random.Range(1, 4)];
        //targetPatrolPoints.ForEach(t => t.GetComponent<SpriteRenderer>().color = Color.white);
        //target.GetComponent<SpriteRenderer>().color = Color.red;
        return target;
    }

    public void OnSpawn(Vector3 _position)
    {
        isDead = false;
        transform.position = _position;
        gameObject.SetActive(true);
        ResetDynamicData();
    }

    void ResetDynamicData()
    {
        simpleDagamageble.ResetValues();
    }

    void OnDeath()
    {
        //TODO: wait for animation
        if (isDead)
            return;

        isDead = true;
        gameObject.SetActive(false);

        if (enemyManager != null)
            enemyManager.OnEnemyDeath(this);
        else
            Debug.Log("enemyManager == null");

        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
    }
}
