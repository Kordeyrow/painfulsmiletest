using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface BaseEnemy
{
    void Setup(EnemyManager _enemyManager, Transform _patrolPointsParent, GameObject _player);
    void OnSpawn(Vector3 pos);
}
