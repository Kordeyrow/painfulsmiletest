using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyManagerData", menuName = "GD/Enemy/EnemyManagerData")]
public class EnemyManagerData : ScriptableObject
{
    [SerializeField] float maxEnemies;
    public float baseSpawnCooldown;
    [SerializeField] int startEnemiesCount;

    public float MaxEnemies => maxEnemies;
    public int StartEnemiesCount => startEnemiesCount;
}
