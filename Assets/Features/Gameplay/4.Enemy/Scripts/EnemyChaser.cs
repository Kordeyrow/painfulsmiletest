using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyChaser : MonoBehaviour, BaseEnemy
{
    [SerializeField] ShipMovementController shipMovementController;
    [SerializeField] EnemyChaserData enemyData;
    [SerializeField] SimpleDamageble simpleDagamageble;
    [SerializeField] GameObject explosionPrefab;

    GameObject player;
    Transform targetPatrolPointsParent;
    EnemyManager enemyManager;

    Transform currentTargetPoint;
    List<Transform> targetPatrolPoints;
    bool isDead;

    bool shouldShoot;

    private void OnEnable()
    {
        simpleDagamageble.Setup(enemyData.SimpleDamagebleData);
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);
    }
    private void OnDisable()
    {
        simpleDagamageble.RegisterOnDeathCallback(OnDeath);
    }
    private void FixedUpdate()
    {
        HandleMovement();
    }

    bool ShouldChase()
    {
        return PlayerInVisionRange();
    }
    bool PlayerInVisionRange()
    {
        if (player == null)
        {
            Debug.Log("player == null");
            return false;
        }
        return Vector2.Distance(player.transform.position, transform.position) < enemyData.VisionRange;
    }
    void Chase()
    {
        shipMovementController.MoveTo(player.transform.position, enemyData.ShipMovementData);
    }

    public void Setup(EnemyManager _enemyManager, Transform _targetPointsParent, GameObject _player)
    {
        enemyManager = _enemyManager;
        targetPatrolPointsParent = _targetPointsParent;
        targetPatrolPoints = GetAllTargetPatrolPoints();
        player = _player;
    }

    List<Transform> GetAllTargetPatrolPoints()
    {
        var targets = new List<Transform>();
        int count = targetPatrolPointsParent.childCount;
        for (int i = 0; i < count; i++)
        {
            var child = targetPatrolPointsParent.GetChild(i);
            targets.Add(child);
        }
        return targets;
    }


    void HandleMovement()
    {
        if (PlayerInVisionRange())
        {
            Chase();
            return;
        }
        Patrol();
    }
    void Patrol()
    {
        if (currentTargetPoint == null)
        {
            currentTargetPoint = NextTargetPoint();
            if (currentTargetPoint == null)
                return;
        }

        bool arrivedOnTargetPoint = Vector3.Distance(transform.position, currentTargetPoint.transform.position) < 0.5f;
        if (arrivedOnTargetPoint)
            currentTargetPoint = NextTargetPoint();

        shipMovementController.MoveTo(currentTargetPoint, enemyData.ShipMovementData);
    }

    Transform NextTargetPoint()
    {
        if (targetPatrolPoints == null)
        {
            Debug.Log("targetPatrolPoints == null");
            return null;
        }
        targetPatrolPoints = targetPatrolPoints.OrderBy(t => Vector2.Distance(t.position, transform.position)).ToList();
        var target = targetPatrolPoints[Random.Range(1, 4)];
        //targetPatrolPoints.ForEach(t => t.GetComponent<SpriteRenderer>().color = Color.white);
        //target.GetComponent<SpriteRenderer>().color = Color.red;
        return target;
    }

    public void OnSpawn(Vector3 _position)
    {
        isDead = false;
        transform.position = _position;
        gameObject.SetActive(true);
        ResetDynamicData();
    }

    void ResetDynamicData()
    {
        simpleDagamageble.ResetValues();
    }

    void OnDeath()
    {
        if (isDead)
            return;

        //TODO: wait for animation

        isDead = true;
        gameObject.SetActive(false);

        if (enemyManager != null)
            enemyManager.OnEnemyDeath(this);
        else
            Debug.Log("enemyManager == null");

        Instantiate(explosionPrefab, transform.position, Quaternion.identity);
    }

    private void OnCollisionEnter2D(Collision2D _col)
    {
        var objectHead = _col.collider.gameObject;
        var colOwner = _col.collider.GetComponent<ColliderOwner>();
        if (colOwner != null && colOwner.Owner != null)
            objectHead = colOwner.Owner;

        OnCollision(objectHead);
    }

    void OnCollision(GameObject _collider)
    {
        if (isDead)
            return;

        if(_collider == player)
        {
            _collider.GetComponent<IDamageble>().ReceiveDamage(enemyData.ImpactDamage);

            //TODO: wait for animation

            OnDeath();
        }
    }
}
