using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointsManager : MonoBehaviour
{
    [SerializeField] EnemyManager enemyManager;
    [SerializeField] TextMeshProUGUI textPoints;
    [SerializeField] TextMeshProUGUI finalTextPoints;

    int currentPoints;

    private void OnEnable()
    {
        enemyManager.OnEnemyDeathEvent += OnPlayerKillEnemy;
    }

    private void OnDisable()
    {
        enemyManager.OnEnemyDeathEvent -= OnPlayerKillEnemy;
    }

    void OnPlayerKillEnemy()
    {
        //Debug.Log(currentPoints);
        currentPoints++;
        //Debug.Log(currentPoints);
        textPoints.text = currentPoints.ToString();
        finalTextPoints.text = currentPoints.ToString();
    }
}
