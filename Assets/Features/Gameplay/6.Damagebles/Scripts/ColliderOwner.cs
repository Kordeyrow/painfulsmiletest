using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderOwner : MonoBehaviour
{
    [SerializeField] GameObject owner;

    public GameObject Owner => owner;
}
