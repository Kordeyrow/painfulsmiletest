using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SimpleDamagebleData
{
    [SerializeField] float maxHealth;

    public float MaxHealth => maxHealth;
}
