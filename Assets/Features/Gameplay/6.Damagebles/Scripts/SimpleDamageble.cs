using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleDamageble : MonoBehaviour, IDamageble
{
    SimpleDamagebleData simpleDamagebleData;
    float __currentHealth;
    bool isDead;
    event UnityAction OnDeathEvent;
    event UnityAction OnChangeHealthEvent;

    public float MaxHealth => simpleDamagebleData.MaxHealth;
    public float CurrentHealth { get { return __currentHealth; } private set { __currentHealth = Mathf.Max(0, Mathf.Min(MaxHealth, value)); OnChangeHealthEvent?.Invoke(); } }
    public float CurrentHealthPercent => CurrentHealth / MaxHealth;

    public void Setup(SimpleDamagebleData _simpleDamagebleData)
    {
        simpleDamagebleData = _simpleDamagebleData;
        ResetValues();
    }

    public void AddHealth(float _value)
    {
        CurrentHealth += _value;
    }

    public void ResetValues()
    {
        isDead = false;
        CurrentHealth = simpleDamagebleData.MaxHealth;
        OnChangeHealthEvent?.Invoke();
    }

    public void ReceiveDamage(float _value)
    {
        if (isDead)
            return;

        CurrentHealth -= _value;

        if (CurrentHealth <= 0)
        {
            //Debug.Log("currentHealth <= 0");
            isDead = true;
            OnDeathEvent?.Invoke();
        }
    }
    public void RegisterOnDeathCallback(UnityAction _callback)
    {
        OnDeathEvent += _callback;
    }

    public void UnregisterOnDeathCallback(UnityAction _callback)
    {
        OnDeathEvent -= _callback;
    }

    public void RegisterOnChangeHealth(UnityAction _callback)
    {
        OnChangeHealthEvent += _callback;
    }

    public void UnRegisterOnChangeHealth(UnityAction _callback)
    {
        OnChangeHealthEvent -= _callback;
    }
}
