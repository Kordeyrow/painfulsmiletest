using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    [SerializeField] Canvas canvas;
    [SerializeField] Slider slider;
    [SerializeField] SimpleDamageble simpleDamageble;
    [SerializeField] SpriteRenderer spriteRenderer;

    [System.Serializable]
    public class CurrentHealthVisuals
    {
        public float percent;
        public Sprite sprite;
    }
    [SerializeField] CurrentHealthVisuals[] currentHealthVisuals;

    private void OnEnable()
    {
        simpleDamageble.RegisterOnChangeHealth(OnChangeHealth);
        OrderHealthVisuals();
    }
    private void Start()
    {
        SetSlider();
    }
    private void Update()
    {
        canvas.transform.LookAt(transform.position + Vector3.forward);
    }
    private void OnDisable()
    {
        simpleDamageble.UnRegisterOnChangeHealth(OnChangeHealth);
    }

    void OrderHealthVisuals()
    {
        currentHealthVisuals = currentHealthVisuals.ToList().OrderByDescending(t => t.percent).ToArray();
    }

    void OnChangeHealth()
    {
        SetSlider();
        UpdateCurrentHealthVisuals();
    }

    void UpdateCurrentHealthVisuals()
    {
        if (currentHealthVisuals.Length <= 0)
            return;

        for (int i = 0; i < currentHealthVisuals.Length; i++)
        {
            if (simpleDamageble.CurrentHealthPercent < currentHealthVisuals[i].percent)
                continue;

            spriteRenderer.sprite = currentHealthVisuals[Mathf.Max(0, i - 1)].sprite;
            return;
        }
        spriteRenderer.sprite = currentHealthVisuals[currentHealthVisuals.Length - 1].sprite;
    }

    void SetSlider()
    {
        slider.value = simpleDamageble.CurrentHealthPercent;
    }
}
