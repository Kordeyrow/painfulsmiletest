using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IDamageble
{
    public void ReceiveDamage(float _value);
    public void RegisterOnChangeHealth(UnityAction _callback);
    public void UnRegisterOnChangeHealth(UnityAction _callback);
    public void RegisterOnDeathCallback(UnityAction _callback);
    public void UnregisterOnDeathCallback(UnityAction _callback);
}
